# Methodology

***Type of Research***

----------
The type of research that will be used in this study is qualitative research and quantitative research. Qualitative researchers aim to gather an in-depth understanding of human behavior and the reasons that govern such behavior. The discipline investigates the “why” and “how” of decision making. Besides this, the researcher will also examine the phenomenon through observations in numerical representations and through statistical analysis. Along with questionnaires that will be given out to respondents for the statistical representation of the findings in the study, interviews with the respondents and a few experts in this field will also be conducted.

***Sampling Method***

----------
The research sampling method that will be used in this study is random sampling to obtain a more scientific result that could be used to represent the entirety of the population. A list of all health care facilities (maternity and lying-in clinics, public and private hospitals, health centers) was acquired from the Las Piñas City Hall.

From 20 barangays, 3 will be picked through random sampling. The health care facilities and institutions in these three barangays will then be the target sources of respondents of the researcher. The health care facilities and institutions will be contacted to obtain a verbal consent to administer the questionnaire to mothers at their places. A letter of consent will also be sent to them along with a sample copy of the questionnaire that will be used, as well as the protocol of the researcher. A letter was also addressed to the City Health Officer to obtain endorsement and consent to conduct a research in selected barangays and distribute questionnaires to the mothers in the vicinity.

Data collection was conducted throughout the facilities‟ and health centers‟ operating hours from Mondays through Sundays in order to include both working and non-working mothers.


***Respondents***

----------
The respondents in this research will all be coming from one single location - Las Piñas City, specifically the randomly selected barangays of Pamplona I, CAA/BF International and Pamplona III.  The researcher chose Las Piñas City because of the socio-economic conditions present in the area that is relevant to the study and also as it fits the time frame and resources of the researcher. The randomly sampled respondents will be asked by the researcher for consent and approval to answer the questionnaire until the desired number of respondents which is 100 is reached. The opinion of experts will also be sought in this research to provide explanations regarding the respondents‟ infant feeding behaviors and practices.

***Questionnaire***

----------
The questionnaire requires information about the socio-economic and demographic background of the mother. It also has questions related to previous infant feeding practices and the birth of her youngest infant and also regarding the baby’s general health and age. Statements that are perceived to be factors that influence mothers‟ infant feeding decisions were presented. The description of the type of infant formula given by formula and mixed feeding mothers will also be asked in the material.
