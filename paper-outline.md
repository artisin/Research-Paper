Outline
======

**Thesis Topic**: A Study on Factors Affecting the Infant Feeding Practices of Mothers in Las Pinas City.

 + **Introduction**
	 1. Statement of the Problem
	 2. Definition of Terms
	 3. Theoretical Framework
	 4. Methodology
		 5. Type of Research
		 6. Respondents
		 7. Questionnaire
	 5. Hypothesis
	 6. Review of Related Literature
	 7. Scope and Limitations
	 8. Significance of the Study
 + **Body**
	 1. Background of the Study
 + **Conclusion**
	 1. Concluding Statement
