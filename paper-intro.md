# Paper Title
#### Introduction
The melamine controversy that erupted during the last quarter of year 2008 brought people’s attention back to the debates between breastfeeding and the use of breast milk substitutes like commercial infant formula. This wasn’t the first time that infant formula had caused illnesses and even deaths to infants worldwide - hence the continuous campaign of World Health Organization (WHO) and UNICEF along with other breastfeeding advocates, for mothers to breastfeed their children at least until 6 months of age. Infant feeding practices refer generally to meet the nutritional and immunological needs of the baby.  A study of infant feeding practices was carried out on a sample of 100 mother and infant pairs. The results revealed that only 20% of mothers in the study currently exclusively breastfeed their babies. 

-------
![Breastfeeding Among Children](http://mchb.hrsa.gov/chusa11/hstat/hsi/downloads/images/204bfVmed.gif)
-------

It also shows that socio-economic factors like mother’s work status, marital status and educational attainment had direct bearing on these practices. Employed mothers tend to cease from breastfeeding their babies and eventually stop and just resort to formula feeding as they go back to work. The study also showed that mothers who are married and living with their partners are more likely to breastfeed their infants than single mothers. Those with higher educational attainment resort more to formula feeding and mixed feeding than those with lower educational attainment. Health care professionals influence mothers the most when it comes to infant feeding decisions.
